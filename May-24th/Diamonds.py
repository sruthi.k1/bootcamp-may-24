import random

RANKS = ["", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
SUITS = ["DIAMOND", "SPADE", "HEART", "CLUB"]
MAX_SUIT_CARDS = 13
LF = "\n"


class Card:
    def __init__(self, suit_name: str, rank: str) -> None:
        self.suit_name = suit_name 
        self.rank = rank

    def __repr__(self) -> str:
        return f'{self.suit_name}: {self.rank}'

class Suit:
    def __init__(self, suit_name: str) -> None:
        self.suit_name = suit_name
        self.suit = self._generate_suit()

    def _generate_suit(self) -> list[Card]:
        return [Card(suit_name, rank) for suit_name, rank in zip([self.suit_name] * MAX_SUIT_CARDS, RANKS[1:])]
            
    def __repr__(self) -> str:
        op = ""
        for ind,card in enumerate(self.suit):
            op += f'{ind}. {card} {LF}'
        return op
    
class Player:
    def __init__(self, name: str, player_count: int) -> None:
        self.name = name
        self.hand = Suit(SUITS[player_count]).suit
        self.cards_played = []
        self.score = 0
        # self.score_cards = []

    def choose_card(self):
        if self.name == "Computer":
            return random.choice(self.hand)
        else:
            print(self.hand)
            return self.hand[int(input("Enter the card number to bid: "))]
 
    def bid(self, top_card: Card) -> Card:   
        print(f'The top card is: {top_card}')
        chosen_card = self.choose_card()
        self.cards_played.append(chosen_card)
        self.hand.remove(chosen_card)
        return chosen_card


class Diamonds:

    DECK = Suit(SUITS[0]).suit

    def __init__(self) -> None:   
        self.players = []
        self.playing_cards = []

    def shuffle_deck(self) -> None:
        random.shuffle(Diamonds.DECK)

    def initilize_players(self) -> None:
        self.players.append(Player("Computer", 1))
        no_of_players = int(input("Enter number of players: "))
        self.players.extend([Player(input("Enter the name of the player: "), _) for _ in range(2, no_of_players)])

    def scoring(self):
        while len(Diamonds.DECK) != 0:
            self.top_card = Diamonds.DECK.pop()
            for player in self.players:
                self.playing_cards.append(player.bid(self.top_card))
            playing_card_ranks = [RANKS.index(card.rank) for card in self.playing_cards]
            
            for p in self.players:
                if RANKS.index(p.cards_played.rank) == max(playing_card_ranks):
                    p.score += RANKS[self.top_card.RANK] /  playing_card_ranks.count(max(playing_card_ranks))
        for p in self.players:
            print(p.score)

    def play(self):
        self.shuffle_deck()
        self.initilize_players()
        self.scoring()


d = Diamonds()
d.play()










