
def initialize_sieve(n):
    is_prime = [True] * (n + 1)
    is_prime[0] = is_prime[1] = False
    return is_prime


def mark_composites(sieve, p):
    for i in range(p * p, len(sieve), p):
        sieve[i] = False


def sieve_of_eratosthenes(n):
    is_prime = initialize_sieve(n)
    for p in range(2, int(n**0.5) + 1):
        if is_prime[p]:
            mark_composites(is_prime, p)

    primes = [p for p in range(2, n + 1) if is_prime[p]]
    return primes


print(sieve_of_eratosthenes(1000))