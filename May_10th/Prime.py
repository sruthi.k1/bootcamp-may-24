def is_prime(n: int) -> bool:
    if n in {2, 3}:
        return True
    if n % 2 == 0:
        return False
    factor = 3
    while factor * factor <= n:
        if n % factor == 0:
            return False
        factor += 2
    return True


print("\n")
print([i for i in range(2, 1000) if is_prime(i)])

# PRIME: 6n +/- 1

def generate_primes(limit: int) -> list[int]:
    def is_prime(n: int) -> bool:
        factor = 5
        while factor * factor <= n:
            if n % factor == 0:
                return False
            factor += 2
        return True

    primes = [2, 3, 5]

    for n in range(6, limit, 6):
        for step in [1, 5]:
            if is_prime(n + step):
                primes.append(n + step)
    return primes


print("\n")
print(generate_primes(1000))
print("\n")


# skiping 2, 3, 5 multiples


def generate_primes(limit: int) -> list[int]:
    def is_prime(n: int) -> bool:
        factor = 7
        while factor * factor <= n:
            if n % factor == 0:
                return False
            factor += 2
        return True

    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]

    for n in range(30, limit, 30):
        for step in [1, 7, 11, 13, 17, 19, 23, 29]:
            if is_prime(n + step):
                primes.append(n + step)
    return primes


print(generate_primes(1000))
print("\n")
